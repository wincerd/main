from lxml import html
import requests
import  bs4
import textblob 
urls = ["http://www.google.com", "http://www.reddit.com","http://www.yahoo.com"]


def parse_websites(list_of_urls):
    for url in list_of_urls:
        html = requests.get(url)
        soup = bs4.BeautifulSoup(html.content,"lxml")
        # kill all script and style elements

        for script in soup(["script", "style"]):
            script.extract()    # rip it out

        # get text
        text = soup.get_text()

        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)

        #print(text)

        wiki = textblob.TextBlob(text)
        r = wiki.sentiment.polarity

        print(r)

parse_websites(urls)
